/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   RBTree.cpp
 * Author: gerson
 * 
 * Created on 26 de Junho de 2018, 17:36
 */
#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_RESET   "\x1b[0m"
#include "RBTree.h"
#include <stdio.h>
#include <deque>
#include <stddef.h>
#include <stddef.h>

RBTree::RBTree() {
    nill = new Node();
    nill->father = root;
    nill->right = nill->left = nill;
}

RBTree::RBTree(const RBTree& orig) {
}

RBTree::~RBTree() {
    root = NULL;
    nill = NULL;
    nill = new Node();
    nill->father = root;
    nill->right = nill->left = nill;
}

void RBTree::insert(Node* q) {
    q->father = nill;
    q->right = nill;
    q->left = nill;

    this->insertRV(&root, q);
}

Node* RBTree::insertRV(Node** rv, Node* q) {
    Node *y = nill;
    Node *x = *rv;
    if ((*rv) == NULL) {// Verifica se a arvore é NULL         
        *rv = q;
        (*rv)->father = nill;
        nill->father = *rv;
        (*rv)->color = 'B'; // propriedade 2
        return 0;
    }
    if ((*rv)->key == q->key) {
        printf(" number already exists !!\n\n");
        return NULL;
    }
    while (x != nill) {// desce na arvore procurando o local para inserir o novo node
        y = x; // armazenar o ultimo node percorrido que vai ser o pai
        if (q->key < x->key) {
            x = x->left;
        } else {
            x = x->right;
        }
    }
    q->father = y;
    if (q->key < y->key) {
        y->left = q; // insere o novo node como filho esquerdo 
    } else if (q->key > y->key) {
        y->right = q; // insere o novo node como filho direito 
    } else if (q->key == y->key) {
        printf(" number already exists !!\n\n");
    }
    q->left = nill;
    q->right = nill;
    q->color = 'R';
    insertColorRV(&(*rv), q);
    return q;
}

void RBTree::insertColorRV(Node **rv, Node *node) {
    Node *y;
    //enquanto existir dois nodes vermelhos seguidos
    //@case 1
    while (node->father->color == 'R') {
        // se o pai do node for o filho esquerdo do seu avo
        if (node->father == node->father->father->left) {
            // y = o filho direito do avô
            y = node->father->father->right;
            // se o filho direito do avô for vermelho @case 2.1
            if (y->color == 'R') {
                //o seu pai fica preto
                node->father->color = 'B';
                //seu avõ fica preto
                y->color = 'B';
                //seu avô fica vermelho
                node->father->father->color = 'R';
                //o node agora é o avô
                node = node->father->father;
            //se o filho direito do avô for preto
            } else {
                //se o node for o filho direito
                if (node == node->father->right) {
                    //node agora é o pai
                    node = node->father;
                    //faz uma rotação esquerda com o pai
                    leftRotation(&(*rv), node);
                }
                //o pai fica preto
                node->father->color = 'B';
                //o avô fica vermelho
                node->father->father->color = 'R';
                //faz uma rotação a direita com o avô
                rightRotation(&(*rv), node->father->father);
            }
        // se o pai do node for o filho direito do seu avo
        } else {
            // y = o filho esquerdo do avô
            y = node->father->father->left;
            //se o filho esquerdo do avô for vermelho
            if (y->color == 'R') {
                // o pai do node fica preto
                node->father->color = 'B';
                //o filho esquerdo do avô fica preto
                y->color = 'B';
                // o avô do no fica vermelho
                node->father->father->color = 'R';
                // node = o seu avô
                node = node->father->father;
            //se o filho esquerdo do avô for preto
            } else {
                //se o node for o filho esquerdo 
                if (node == node->father->left) {
                    //o node agora é o pai
                    node = node->father;
                    //faz uma rotacao a direita como o pai 
                    rightRotation(&(*rv), node);
                }
                // o pai do node fica preto
                node->father->color = 'B';
                //o avô do node fica vermelho
                node->father->father->color = 'R';
                //faz uma rotação a esquerda com o avô do node
                leftRotation(&(*rv), node->father->father);
            }
        }
    }
    //propriedade 2 
    (*rv)->color = 'B';
}

void RBTree::leftRotation(Node **rv, Node *node) {
    Node *y;
    if (((*rv)->father == nill) && (node->right != nill)) {
        y = node->right;
        node->right = y->left;
        y->left->father = node;
        y->father = node->father;
        if (node->father == nill) {
            *rv = y;
        } else if (node == node->father->left) {
            node->father->left = y;
        } else {
            node->father->right = y;
        }
    }
    y->left = node;
    node->father = y;
    (*rv)->father = nill;
}

void RBTree::rightRotation(Node **rv, Node* node) {
    Node *y;
    if (((*rv)->father == nill) && (node->left != nill)) {
        y = node->left;
        node->left = y->right;
        y->right->father = node;
        y->father = node->father;
        if (node->father == nill) {
            *rv = y;
        } else if (node == node->father->right) {
            node->father->right = y;
        } else {
            node->father->left = y;
        }
    }
    y->right = node;
    node->father = y;
    (*rv)->father = nill;
}

void RBTree::printTreePreOrder() {
    if (root != NULL) {
        preOrder(root);
        printf("%s\n", ANSI_COLOR_RESET);
    } else {
        printf("Tree RN is empty \n");
    }
}

void RBTree::inLevel(Node* node) {
    if (node != NULL) {
        std::deque<Node*> fila;
        fila.push_back(node);
        while (!fila.empty()) {

            Node* no_atual = fila.front();
            fila.pop_front();

            printNode(no_atual);
            if (no_atual->left != nill)
                fila.push_back(no_atual->left);
            if (no_atual->right != nill)
                fila.push_back(no_atual->right);
        }
    } else {
        printf("Tree RN is empty \n");
    }
}

void RBTree::printTreeInLevel() {
    if (root != NULL) {
        inLevel(root);
        printf("%s\n", ANSI_COLOR_RESET);
    } else {
        printf("Tree RN is empty \n");
    }
}

void RBTree::preOrder(Node* rv) {
    if (rv != nill) {
        printNode(rv);
        preOrder(rv->left);
        preOrder(rv-> right);
    }
}

void RBTree::printNode(Node* node) {
    if (node->color == 'R') {
        printf("%s %d ", ANSI_COLOR_RED, node->key);
    } else {
        printf("%s %d ",ANSI_COLOR_RESET , node->key);
    }
}

int RBTree::getSize() {
    return sizeBlack(root);
}

int RBTree::sizeBlack(Node* node) {
    int heightLeft = 0, heightRight = 0;
    if (!node) {
        return 0;
    }
    if (node == nill) {
        return 1;
    }
    if (node->color == 'B') {
        heightLeft += sizeBlack(node->left);
        heightRight += sizeBlack(node->right);
    } else {
        sizeBlack(node->left);
        sizeBlack(node->right);
    }
    if (heightLeft > heightRight) {
        return heightLeft + 1;
    } else {
        return heightRight + 1;
    }
}

void RBTree::remove(int key) {
    if (root != NULL) {
        Node *temp = searchRV(root, key);
        if (temp->key == key) {
            printNode(temp);
            printf("%s\n", ANSI_COLOR_RESET);
            removeRV(&root, temp);
        } else {
            printf("\nThe number %d not exist!\n\n", key);
        }
        if (root == nill) {
            nill = new Node();
            nill->father = root = NULL;
            nill->right = nill->left = nill;
        }
    } else {
        printf("Tree is empty \n");
    }
}

Node* RBTree::getNext(Node *node) {
    Node *temp;
    if (node->right != nill) {
        return getMin(node->right);
    }
    temp = node->father;
    while ((temp != nill) && (node == temp->right)) {
        node = temp;
        temp = temp->father;
    }
}

Node* RBTree::getMin(Node *node) {
    while (node->left != nill) {
        node = node->left;
    }
    return node;
}

Node* RBTree::removeRV(Node **rv, Node* node) {
    Node *y, *x;
    //node folha
    if ((node->left == nill) || (node->right == nill)) {
        y = node;
    //pegar o proximo menor
    } else {
        y = getNext(node);
    }
    //se tiver um filho a esquerda
    if (y->left != nill) {
        x = y->left;
    //se tiver um filho a direita
    } else {
        x = y->right;
    }
    x->father = y->father;
    //se o pai for null a raiz da subarvore vai ser o node y
    if (y->father == nill) {
        *rv = x;
    //se y for um filho esquerdo
    } else if (y == y->father->left) {
        y->father->left = x;
    //se for direito
    } else {
        y->father->right = x;
    }
    
    if (y != node) {
        node->key = y->key;
    }//Aqui os dados sao transferidos     
    if (y->color == 'B') {
        if ((*rv)->right == nill && (*rv)->left->right != nill) {
            leftRotation(&(*rv), (*rv)->left);
            removeColorRV(&(*rv), (*rv)->left);
            rightRotation(&(*rv), (*rv));
        } else {
            if ((*rv)->left == nill && (*rv)->right->left != nill) {
                rightRotation(&(*rv), (*rv)->right);
                removeColorRV(&(*rv), (*rv)->right);
                leftRotation(&(*rv), (*rv));
            }
        }
        removeColorRV(&(*rv), x);
    }
    return y;
    delete y;
    delete x;
}

void RBTree::removeColorRV(Node **rv, Node* node) {
    Node *temp;
    //enquanto o node não for a raiz e sua cor for preta
    while (((*rv) != node) && (node->color == 'B')) {
        //se o node for o filho esquerdo
        if (node == node->father->left) {
            // o node temporaio recebe o filho direito do pai
            temp = node->father->right;
            //se a cor do node temporario for vermelha 
            if (temp-> color == 'R') {
                //seta sua cor para preto
                temp->color = 'B';
                //a cor do seu pai fica vermelha
                node->father->color = 'R';
                //faz uma rotação esquerda com o pai 
                leftRotation(&(*rv), node->father);
                // o node temporario agora e o filho direito do pai do node
                temp = node->father->right;
            }
            //se a cor do filho esquerdo e do direito for preta
            if ((temp->left->color == 'B') && (temp->right->color == 'B')) {
                //a cor do temp fica vermelha
                temp->color = 'R';
                // o node agora é o pai
                node = node->father;
            //se a cor do filho direito for preta e a do filho esquerdo for vermelha
            } else if (temp->right->color == 'B') {
                //seta a cor do filho esquerdo para preto
                temp->left->color = 'B';
                //e sua cor para vermelha
                temp->color = 'R';
                //faz uma rotação a direita com o node temporario
                rightRotation(&(*rv), temp);
                // o node temporaio agora e o filho direito do node
                temp = node->father->right;
                //a cor de temp e a cor do pai do node
                temp->color = node->father->color;
                //o pai fica preto
                node->father->color = 'B';
                //o filho direito de tem fica preto
                temp->right->color = 'B';
                //faz uma rotação com o pai do node
                leftRotation(&(*rv), node->father);
                node = *rv;
            }
        //se for o filho direito
        } else {
            //temp agora é o filho esquerdo do pai
            temp = node->father->left;
            // a cor do filho esquerdo do pai for vermelha
            if (temp->color == 'R') {
                //fica preta
                temp->color = 'B';
                //o pai fica vermelho
                node->father->color = 'R';
                //faz uma rotação a direita com o pai
                rightRotation(&(*rv), node->father);
                //temp agora é o filho da esquerda do node
                temp = node->father->left;
            }
            //se os filhos tiverem cor preta
            if ((temp->left->color == 'B') && (temp->right->color == 'B')) {
                // ele fica vermelho
                temp->color = 'R';
                //o node agora é o pai
                node = node->father;
            //se só o da esquerda for preto
            } else if (temp->left->color == 'B') {
                //o da direita fica preto
                temp->right->color = 'B';
                temp->color = 'R';
                leftRotation(&(*rv), temp);
                temp = node->father->left;
                temp->color = node->father->color;
                node->father->color = 'B';
                temp->left ->color = 'B';
                rightRotation(&(*rv), node->father);
                node = *rv;
            }
        }
    }
    node->color = 'B';
}

void RBTree::search(int key) {
    if (root != NULL) {
        Node* node = searchRV(root, key);
        if (node->key == key) {
            printNode(node);
            delete node;
        } else {
            printf("\nThe number %d not exist!\n\n", key);
        }
    } else {
        printf("Tree RN is empty \n");
    }

}

Node* RBTree::searchRV(Node* node, int key) {
    if ((node == nill) || (node->key == key)) {
        return node;
    }
    if (key < node->key) {
        return searchRV(node->left, key);
    } else {
        return searchRV(node->right, key);
    }
}