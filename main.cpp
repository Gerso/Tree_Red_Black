/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: gerson
 * Reference project site: https://www.passeidireto.com/arquivo/10803587/arvores-rubro-negras---trabalho-final
 * Created on 26 de Junho de 2018, 16:05
 */

#include <cstdlib>
#include <stdio.h>

#include "Node.h"
#include "RBTree.h"
using namespace std;

const int vector1[] = {7, 2, 11, 1, 5, 8, 14, 4, 13, 15};

int menu() {
    int opcao;
    printf("--------   1 - Insert   --------\n");
    printf("--------   2 - PreOrder --------\n");
    printf("--------   3 - InLevel  --------\n");
    printf("--------   4 - Size     --------\n");
    printf("--------   5 - Delete   --------\n");
    printf("--------   6 - Search   --------\n");
    printf("--------   7 - Clear    --------\n");
    printf("--------   0 - Exit     --------\n");
    scanf("%d", &opcao);
    return opcao;
}

int main() {

    int value, option;
    RBTree *rb = new RBTree();
    Node *newNode;

    for (int i = 0; i < 10; i++) {
        newNode = new Node();
        newNode->key = vector1[i];
        newNode->color = 'R'; // propriedade 4
        rb->insert(newNode);
    }

    do {
        option = menu();
        switch (option) {
            case 1:
                newNode = new Node();
                printf("Enter a value to insert: ");
                scanf("%d", &newNode->key);
                newNode->color = 'R'; // propriedade 4     
                rb->insert(newNode);
                break;
            case 2:
                printf("print tree RN in Pre Order \n\n");
                rb->printTreePreOrder();
                break;
            case 3:
                printf("print tree RN in Level \n\n");
                rb->printTreeInLevel();
                break;
            case 4:
                printf("Size tree RN is: %d\n\n", rb->getSize());
                break;
            case 5:
                printf("Enter a value to remove: ");
                scanf("%d", &value);
                rb->remove(value);
                break;
            case 6:
                printf("Enter a value to search: ");
                scanf("%d", &value);
                rb->search(value);
                break;
            case 7:
                printf("Clear TreeRB \n\n");
                rb->~RBTree();
                break;

        }
    } while (option != 0);
    delete newNode;
    delete rb;
    return 0;
}