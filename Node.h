/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Node.h
 * Author: gerson
 * Class node that composes the values ​​of the black red binary tree
 * Created on 26 de Junho de 2018, 16:08
 */

#ifndef NODE_H
#define NODE_H

class Node {
public:
    Node();
    Node(const Node& orig);
    virtual ~Node();
    
    /**
     * Value the node 
     */
    int key;
    
    /**
     * Color red = 'R' or black = 'B' that will be assigned to the node
     */
    char color;
    
    /**
     * Address of the parent of the current node
     */
    Node *father;
    
    /**
     * Address of the left child of the current node
     */
    Node *left;
    
    /**
     * Address of the right child of the current node
     */
    Node *right;
private:

};

#endif /* NODE_H */

