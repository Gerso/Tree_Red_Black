/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   RBTree.h
 * Author: gerson
 *
 * Created on 26 de Junho de 2018, 17:36
 */

#ifndef RBTREE_H
#define RBTREE_H

#include <stddef.h>

#include "Node.h"

class RBTree {
public:
    
    /**
     * Inicia um node vazio e seta seu pai para nulo e seus filhos esquerdo e direito para apontarem para si
     */
    RBTree();
    
    /**
     * 
     * @param orig
     */
    RBTree(const RBTree& orig);
    
    /**
     * 
     */
    virtual ~RBTree();
    
    /**
     * Recebe um node do main e seta seu pai e seus filhos para apontarem para o node vazio
     * @param node = todo node quando criado é vermelho
     */
    void insert(Node *q);
    
    /**
     * Public method for call private method para exibir a arvore em pre_Order
     */
    void printTreePreOrder();
    
    /**
     * Public method for call private method para exibir a arvore por nivel
     */
    void printTreeInLevel();
    
    /**
     * 
     * @return currtent size
     */
    int getSize();
    
    /**
     * Public method for call private method para pesquisar o valor na arvore 
     * @param key
     */
    void search(int key);
    
    /**
     * Private method para pesquisar o valor na arvore
     * @param key
     */
    void remove(int key);
private:
    
    /**
     * Recebe o endereco da raiz da arvore e insere o novo no caso ele não exista 
     * @param rv = raiz da arvore
     * @param node = novo node a ser inserido
     * @return 0 se a arvore for null, null se o node ja existir, @node atual inserido 
     */
    Node *insertRV(Node **rv, Node *q);
    
    /**
     * Recebe o endereco da raiz e o node que foi inserido e faz o jogo de cores e suas rotações caso necessario 
     * @param root = raiz da arvore
     * @param node = node que foi inserido
     */
    void insertColorRV(Node **rv, Node *node);
    
    /**
     * faz uma rotação a esquerda quando um node e inserido em uma subArvore direita de uma subArvore direita
     * @param root = tree root 
     * @param node = node inserted
     */
    void leftRotation(Node **rv, Node *node);
    
    /**
     * faz uma rotação a dierita quando um node e inserido em uma subArvore esquerda de uma subArvore esquerda
     * @param root = tree root
     * @param node = node inserted
     */
    void rightRotation(Node **rv, Node *node);
    
    /**
     * metodo para mostrar a arvore em pre-Order
     * @param node = tree root
     */
    void preOrder(Node* node);
    
    /**
     * metodo para mostrar a arvore por nivel
     * @param node = tree root
     */
    void inLevel(Node * node);
    
    /**
     * method for print node and sua respectiva cor
     * @param node
     */
    void printNode(Node* node);
    
    /**
     * 
     * @param node = tree root 
     * @param key = key a ser procurada
     * @return node if exist else return null
     */
    Node *searchRV(Node* node, int key);
    
    /**
     * 
     * @param root 
     * @param node
     * @return 
     */
    Node *removeRV(Node **rv, Node *node);
    
    /**
     * Recebe o endereco da raiz e o node que foi inserido e faz o jogo de cores e suas rotações caso necessario
     * @param root
     * @param node
     */
    void removeColorRV(Node **rv,Node *node);
    
    /**
     * 
     * @param node = tree root
     * @return return a aluta negra da arvore
     */
    int sizeBlack(Node *node);
    
    /**
     * method para retorar o sucessor
     * @param node
     * @return  getMin(Node *node)
     */
    Node* getNext(Node *node);
    
    /**
     * localizar o menor node
     * @param node
     * @return return the menor node
     */
    Node* getMin(Node *node);
    
    /**
     * node empty 
     */
    Node *nill = NULL;
    
    /**
     * vareavel para armazenar a raiz da arvore
     */
    Node *root = NULL;
};

#endif /* RBTREE_H */